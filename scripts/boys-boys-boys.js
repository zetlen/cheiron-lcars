var OAuth2 = require('oauth').OAuth2;
var https = require('https');
var qs = require('querystring');
var Promise = require('lie');

var key = process.env.HUBOT_TWITTER_CONSUMER_KEY,
    secret = process.env.HUBOT_TWITTER_CONSUMER_SECRET,
    jess = 'panicea';

module.exports = function(robot) {
  robot.hear(/^\s*boys\s+boys\s+boys\s*$/i, function(msg) {
    obtainAndCacheToots(jess, robot.brain.data).then(function(toots) {
      var toot = msg.random(toots);
      imageMe(msg, toot.text, Math.random() > 0.9, function(url) {
        msg.send('http://twitter.com/' + jess + '/status/' + toot.id_str);
        msg.send(url);
      })
    }).catch(console.error);
  });
}

function obtainAndCacheToots(user, storage) {
  storage.toots = storage.toots || {};
  var storedToots = storage.toots[user] || [];
  function storeToots(toots) {
    storage.toots[user] = toots.concat(storedToots);
    return storage.toots[user];
  }
  if (storedToots.length === 0) {
    return getAllTweetsForUser(user).then(storeToots);
  } else {
    return getAdditionalTweetsForUser(user, storedToots).then(storeToots);
  }
}

function getMinId(tweets) {
  return tweets.reduce(function(min, next) {
    return next.id < min.id ? next : min;
  }).id;
}

function getMaxId(tweets) {
  return tweets.reduce(function(max, next) {
    return next.id > max.id ? next : max;
  }).id
}

function getAdditionalTweetsForUser(user, tweets) {
  return getAllTweetsForUser(user, {
    since_id: getMaxId(tweets)
  });
}

function getAllTweetsForUser(user, options) {
  var maxPageSize = 200;
  function getPage(max, soFar) {
    var settings = {
      count: maxPageSize,
      screen_name: user
    };
    if (options && options.since_id) settings.since_id = options.since_id;
    if (max) settings.max_id = max;
    return getTweets(key, secret, settings).then(function(res) {
      if (res.length < maxPageSize) {
        return soFar.concat(res);
      } else {
        return getPage(getMinId(res), soFar.concat(res));
      }
    })
  }
  return getPage(null, []);
}

function getTweets(KEY, SECRET, settings) {
  var oauth2 = new OAuth2(
    KEY, 
    SECRET, 
    'https://api.twitter.com/', 
    null, 
    'oauth2/token', 
    null
  );

  return new Promise(function(resolve, reject) {
    oauth2.getOAuthAccessToken('', {
      'grant_type': 'client_credentials'
    }, function(e, access_token) {

      var options = {
        hostname: 'api.twitter.com',
        path: '/1.1/statuses/user_timeline.json?' + qs.stringify(settings),
        headers: {
          Authorization: 'Bearer ' + access_token
        }
      };

      var req = https.get(options, function(result) {
        var buffer = '';
        // result.setEncoding('utf8');
        result.on('data', function(data) {
          buffer += data;
        });
        result.on('end', function() {
          var parsed;
          try {
            parsed = JSON.parse(buffer);
          } catch(e) {
            reject(new Error("Error parsing response, invalid JSON: " + buffer));
          }
          if (result.statusCode >= 400 && result.statusCode < 600) {
            if (parsed.errors) {
              if (parsed.errors.length === 1) {
                var e = new Error(parsed.errors[0].message);
                e.code = parsed.errors[0].code;
                reject(e);
              } else {
                reject(new Error("Multiple errors occurred:\n" + JSON.stringify(parsed.errors, null, 2)));
              }
            }
          } else {
            resolve(parsed);
          }
        });
        req.on('error', reject);
        result.on('error', reject);
      });
    });
  });
}

var ensureImageExtension, imageMe;

imageMe = function(msg, query, animated, faces, cb) {
  var googleApiKey, googleCseId, q, url;
  if (typeof animated === 'function') {
    cb = animated;
  }
  if (typeof faces === 'function') {
    cb = faces;
  }
  googleCseId = process.env.HUBOT_GOOGLE_CSE_ID;
  if (googleCseId) {
    googleApiKey = process.env.HUBOT_GOOGLE_CSE_KEY;
    if (!googleApiKey) {
      msg.robot.logger.error("Missing environment variable HUBOT_GOOGLE_CSE_KEY");
      msg.send("Missing server environment variable HUBOT_GOOGLE_CSE_KEY.");
      return;
    }
    q = {
      q: query,
      searchType: 'image',
      safe: 'high',
      fields: 'items(link)',
      cx: googleCseId,
      key: googleApiKey
    };
    if (typeof animated === 'boolean' && animated === true) {
      q.fileType = 'gif';
      q.hq = 'animated';
    }
    if (typeof faces === 'boolean' && faces === true) {
      q.imgType = 'face';
    }
    url = 'https://www.googleapis.com/customsearch/v1';
    return msg.http(url).query(q).get()(function(err, res, body) {
      var error, i, image, len, ref, ref1, response, results;
      if (err) {
        msg.send("Encountered an error :( " + err);
        return;
      }
      if (res.statusCode !== 200) {
        msg.send("Bad HTTP response :( " + res.statusCode);
        return;
      }
      response = JSON.parse(body);
      if (response != null ? response.items : void 0) {
        image = msg.random(response.items);
        return cb(ensureImageExtension(image.link));
      } else {
        msg.send("Oops. I had trouble searching '" + query + "'. Try later.");
        if ((ref = response.error) != null ? ref.errors : void 0) {
          ref1 = response.error.errors;
          results = [];
          for (i = 0, len = ref1.length; i < len; i++) {
            error = ref1[i];
            results.push((function(error) {
              msg.robot.logger.error(error.message);
              if (error.extendedHelp) {
                return msg.robot.logger.error("(see " + error.extendedHelp + ")");
              }
            })(error));
          }
          return results;
        }
      }
    });
  } else {
    q = {
      v: '1.0',
      rsz: '8',
      q: query,
      safe: 'active'
    };
    if (typeof animated === 'boolean' && animated === true) {
      q.imgtype = 'animated';
    }
    if (typeof faces === 'boolean' && faces === true) {
      q.imgtype = 'face';
    }
    return msg.http('https://ajax.googleapis.com/ajax/services/search/images').query(q).get()(function(err, res, body) {
      var image, images, ref;
      if (err) {
        msg.send("Encountered an error :( " + err);
        return;
      }
      if (res.statusCode !== 200) {
        msg.send("Bad HTTP response :( " + res.statusCode);
        return;
      }
      images = JSON.parse(body);
      images = (ref = images.responseData) != null ? ref.results : void 0;
      if ((images != null ? images.length : void 0) > 0) {
        image = msg.random(images);
        return cb(ensureImageExtension(image.unescapedUrl));
      }
    });
  }
};

ensureImageExtension = function(url) {
  var ext;
  ext = url.split('.').pop();
  if (/(png|jpe?g|gif)/i.test(ext)) {
    return url;
  } else {
    return url + "#.png";
  }
};