
# Description:
#   Allows Hubot to roll dice
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   hubot roll (die|one) - Roll one six-sided dice
#   hubot roll dice - Roll two six-sided dice
#   hubot roll <x>d<y> - roll x dice, each of which has y sides
#
# Author:
#   ab9


good = [
  "I wrote a hit play and directed it, so I'm not sweating it either."
  "I like your nurse's uniform, guy."
  "I'll send him back to Ireland in a bodybag."
  "My top schools where I want to apply to are Oxford and the Sorbonne. My safety's Harvard."
  "...And you know what else? It was worth it."
  "I always wanted to be in one of your fuckin' plays."
  "Best play ever, man."
  "Will you marry me, Le-Chahn?"
]

bad = [
  "With friends like you, who needs friends?"
  "Never in my wildest imagination did I ever dream I would have sons like these."
  "I was punched in the face. What's your excuse?"
  "Sic transit gloria.  'Glory fades'."
  "You think I got kicked out because of just the aquarium? Nah, it was the handjob..."
  "I just came by to thank you for WRECKING MY LIFE."
]

zerofuck = [
  "I'm not sure if I know how to roll a zero-sided die."
  "You'll get nothing and like it."
  "I'm so tired."
  "Cool joke. Zero sided dice. I get it."
  "I rolled infinitesimal points and the result was indeterminate."
]

onefuck = [
  "Can you draw me a picture of a one-sided die?"
  "I only roll Mobius dice if I'm deep in the Klein bottle."
  "Look in the mirror. What have you become?"
  "You're a transcendent genius and you hacked the bot."
  "I'm so tired."
]

hundredfuck = [
  "I'm not going to roll more than a hundred dice for you."
  "Sorry, my pouch is too small."
  "I'll only roll more than 100 dice if you promise to eat each die that comes up 1."
  "You think I've got all day, jerkwad? I've got places to be!"
  "'Undreds o'dice? Oi, yer a right bell end, m8"
]

nonefuck = [
  ". What was the point of that? Or anything?"
  " and I looked amazing."
  " because you told me not to, for some reason."
  " and things turned out very much as before."
  ". The results were indeterminate."
  " and nothing failed to continue to happen."
]

yafucks = [
  "ＹＡ ＦＵＣＫＳ"
  "jå føx"
  "Ia, Phx"
  "Y͔̳̩A̮͞ ̦F̝̠U̹̮̺͇̘͓̗C͞K͈̥͓S̳͖"
  "كنت الملاعين"
  "na ị na-emejọ"
  ":sheep: :wolf:"
  "j00 pHuX0rz"
]

article = (n) ->
  if n == 11 || n == 18 || n.toString().charAt(0) == "8" then "an" else "a"

rnd = (coll) ->
  coll[Math.floor(Math.random() * coll.length)]

ScoreKeeper = require('./scorekeeper');

critHit = ":tada: CRITICAL HIT :tada:"
critFail = ":sad_pepe: CRITICAL MISS :sad_pepe:"
from = "THE HANDS OF FATE"

module.exports = (robot) ->
  keeper = new ScoreKeeper(robot)

  rollReply = (leader, name, score, reasonScore, from) ->
    if score?
      "#{leader} #{name} has #{score} points, #{reasonScore} of which #{from} ordained."
    else
      "All right, all right, quit it for a while."

  rollFor = (name, room) ->
    if (name.charAt(0) != ":")
      name = (name.replace /(^\s*@)|([,:\s]*$)/g, "").trim().toLowerCase()
    result = rollOne 20
    reported = report [result]
    if result == 1
      [score, reasonScore] = keeper.subtract(name, from, room, critFail, 100)
      rollReply "#{reported} #{critFail}", name, score, reasonScore, from
    else if result == 20
      [score, reasonScore] = keeper.add(name, from, room, critHit, 100)
      rollReply "#{reported} #{critHit}", name, score, reasonScore, from
    else if result < 4
      [score, reasonScore] = keeper.subtract(name, from, room, from)
      rollReply reported, name, score, reasonScore, from
    else if result > 16
      [score, reasonScore] = keeper.add(name, from, room, from)
      rollReply reported, name, score, reasonScore, from
    else
      "#{reported} Nothing happened#{rnd nonefuck}"

  robot.respond /roll for ([\s\w'@.-:]*)/i, (msg) ->
    msg.reply rollFor(msg.match[1], msg.message.room)

  robot.hear ///
    ^\s*ya\s*fucks\s*$
  ///i, (msg) ->
    msg.send ":rotating_light: #{msg.random yafucks} :rotating_light:"
    msg.send "Rolling a d20 for #{msg.message.user.name}... #{rollFor(msg.message.user.name, msg.message.room)}" 

  robot.respond /roll (die|one)/i, (msg) ->
    msg.reply report [rollOne(6)]
  robot.respond /roll dice/i, (msg) ->
    msg.reply report roll 2, 6
  robot.respond /flip a coin/i, (msg) ->
    msg.reply coinReport roll 1, 2
  robot.respond /flip (\d+) coins/i, (msg) ->
    coins = msg.match[1];
    msg.reply coinReport roll coins, 2
  robot.respond /roll (\d+)d(\d+)/i, (msg) ->
    dice = parseInt msg.match[1]
    sides = parseInt msg.match[2]
    if sides < 1
      answer = msg.random zerofuck
    else if sides == 1
      answer = msg.random onefuck
    else if dice > 100
      answer = msg.random hundredfuck
    else
      res = roll dice, sides
      total = totalRes res
      succ = total / (res.length * sides)
      rpt = if sides == 2
        coinReport res
      else
        report res
      answer = if dice == 0
        rpt
      else if succ >= 0.9
        rpt + ' ' + msg.random good
      else if succ <= 0.1 || total == res.length
        rpt + ' ' + msg.random bad
      else
        rpt
    msg.reply answer

totalRes = (res) ->
  res.length == 0 ? 0 : res.reduce (x, y) -> x + y

report = (results) ->
  if results?
    switch results.length
      when 0
        "I didn't roll any dice" + rnd nonefuck
      when 1
        "I rolled #{article(results[0])} #{results[0]}."
      else
        total = totalRes results
        finalComma = if (results.length > 2) then "," else ""
        last = results.pop()
        "I rolled #{results.join(", ")}#{finalComma} and #{last}, making #{total}."

coinReport = (results) -> 
  if results?
    binaryResults = results.map (x) -> --x
    namedResults = binaryResults.map (x) -> x && "heads" || "tails"
    switch results.length
      when 0
        "I flipped zero coins" + rnd nonefuck
      when 1
        "It came up #{namedResults[0]}."
      else
        total = totalRes binaryResults
        finalComma = if (results.length > 2) then "," else ""
        last = namedResults.pop()
        "They came up #{namedResults.join(", ")}#{finalComma} and #{last}, making #{total} heads."

roll = (dice, sides) ->
  rollOne(sides) for i in [0...dice]

rollOne = (sides) ->
  1 + Math.floor(Math.random() * sides)