# Description:
# First line of AR Trek 1.
#
# Commands:
# computer rebirth
#

module.exports = (robot) ->
  robot.respond /rebirth/i, (msg) ->
    msg.send "Congratulations -- your transfer to the Centaur-class USS Cheiron (NCC-40220-A) has been approved. You will be both a fine and welcome addition to the crew, which stands ready to engage primarily in missions of exploration in both stellar cartography and science, involving some mitigation of diplomatic insurgence along the Demilitarized Zone.";